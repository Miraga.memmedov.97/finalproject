package az.ingressMs19.eTaskIfy.controller;

import az.ingressMs19.eTaskIfy.dto.OrganizationDto;
import az.ingressMs19.eTaskIfy.dto.OrganizationRequest;
import az.ingressMs19.eTaskIfy.service.OrganizationService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("api/v1/organizations")
@RequiredArgsConstructor
public class OrganizationController {

    private final OrganizationService organizationService;

    @GetMapping("/{id}")
    public OrganizationDto getById(@PathVariable long id) {
        return organizationService.getOrganizationById(id);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void updateOrganization(@PathVariable long id,
                                   @Valid @RequestBody OrganizationRequest organizationRequest) {
        organizationService.updateOrganization(id, organizationRequest);
    }
}
