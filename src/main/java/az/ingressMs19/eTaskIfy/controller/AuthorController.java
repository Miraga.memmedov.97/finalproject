package az.ingressMs19.eTaskIfy.controller;

import az.ingressMs19.eTaskIfy.dto.authDto.AuthResponse;
import az.ingressMs19.eTaskIfy.dto.authDto.SignInRequest;
import az.ingressMs19.eTaskIfy.dto.authDto.SignUpRequest;
import az.ingressMs19.eTaskIfy.exception.IncorrectCredentialsException;
import az.ingressMs19.eTaskIfy.service.auth.AuthService;
import az.ingressMs19.eTaskIfy.service.auth.JwtService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/auth")
@RequiredArgsConstructor
public class AuthorController {
    @Autowired
    private final AuthService authService;

    @PostMapping("/sign-up")
    public ResponseEntity<AuthResponse> signUp(@Valid @RequestBody SignUpRequest signUpRequest) {
        return ResponseEntity.ok(authService.signUp(signUpRequest));
    }
    @PostMapping("/sign-in")
    public ResponseEntity<?> signIn(@Valid @RequestBody SignInRequest request) {
        try{
            return ResponseEntity.ok(authService.signIn(request));
        } catch (IncorrectCredentialsException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
        }
    }
}
