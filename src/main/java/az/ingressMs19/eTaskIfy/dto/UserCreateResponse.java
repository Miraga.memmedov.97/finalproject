package az.ingressMs19.eTaskIfy.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema
public class UserCreateResponse {
    private String email;
    private String password;
}
