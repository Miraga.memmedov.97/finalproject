package az.ingressMs19.eTaskIfy.dto.authDto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SignInRequest {
    @Email(message = "Email should be valid")
    private String email;
    @Size(min = 6, max = 50, message = "The minimum password length must be 6")
    private String password;
}
