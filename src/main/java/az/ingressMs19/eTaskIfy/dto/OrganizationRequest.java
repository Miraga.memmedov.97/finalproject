package az.ingressMs19.eTaskIfy.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema
public class OrganizationRequest {
    @NotBlank
    private String organizationName;
    private String address;
    private String phoneNumber;
}
