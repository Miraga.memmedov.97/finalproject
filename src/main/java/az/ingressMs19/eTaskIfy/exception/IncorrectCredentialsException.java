package az.ingressMs19.eTaskIfy.exception;

public class IncorrectCredentialsException extends RuntimeException{
    public IncorrectCredentialsException(String message){
        super(message);
    }
}
