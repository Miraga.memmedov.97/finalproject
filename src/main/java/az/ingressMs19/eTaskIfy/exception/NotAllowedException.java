package az.ingressMs19.eTaskIfy.exception;

public class NotAllowedException extends RuntimeException{
    public NotAllowedException(String message){
        super(message);
    }
}
