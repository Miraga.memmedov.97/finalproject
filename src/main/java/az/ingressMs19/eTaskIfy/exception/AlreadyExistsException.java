package az.ingressMs19.eTaskIfy.exception;

public class AlreadyExistsException extends RuntimeException{
    public AlreadyExistsException(String massage){
        super(massage);
    }
}
