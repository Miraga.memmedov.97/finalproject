package az.ingressMs19.eTaskIfy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ETaskIfyApplication {

	public static void main(String[] args) {
		SpringApplication.run(ETaskIfyApplication.class, args);
	}

}
