package az.ingressMs19.eTaskIfy.enums;

public enum TaskStatus {
    PENDING,
    IN_PROGRESS,
    COMPLETED,
    OVERDUE
}
