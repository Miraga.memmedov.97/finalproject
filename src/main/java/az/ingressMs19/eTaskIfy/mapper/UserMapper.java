package az.ingressMs19.eTaskIfy.mapper;

import az.ingressMs19.eTaskIfy.dto.UserDto;
import az.ingressMs19.eTaskIfy.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class UserMapper {
    private final TaskMapper taskMapper;

    public UserDto mapUserToUserDTO(User user) {
        return UserDto.builder()
                .email(user.getEmail())
                .name(user.getName())
                .surname(user.getSurname())
                .username(user.getUsername())
                .tasks(user.getTasks()
                        .stream()
                        .map(taskMapper::mapTaskToDto)
                        .collect(Collectors.toSet()))
                .build();

    }
}
