package az.ingressMs19.eTaskIfy.mapper;

import az.ingressMs19.eTaskIfy.dto.TaskDto;
import az.ingressMs19.eTaskIfy.model.Task;
import org.springframework.stereotype.Component;

@Component
public class TaskMapper {
    public TaskDto mapTaskToDto(Task task) {
        return TaskDto.builder()
                .title(task.getTitle())
                .description(task.getDescription())
                .status(task.getStatus())
                .deadline(task.getDateTime())
                .build();
    }
}
