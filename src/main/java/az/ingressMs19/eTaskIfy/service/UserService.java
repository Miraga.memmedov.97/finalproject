package az.ingressMs19.eTaskIfy.service;

import az.ingressMs19.eTaskIfy.dto.UserCreateResponse;
import az.ingressMs19.eTaskIfy.dto.UserDto;
import az.ingressMs19.eTaskIfy.dto.UserRequest;

import java.util.List;

public interface UserService {
    UserCreateResponse addUser(UserRequest userRequest);

    List<UserDto> getAllUsers();

    UserDto getUserById(Long id);

    UserDto getUserByEmail(String email);

    void deleteUserById(Long id);

    void updateUserById(Long id, UserRequest userRequest);
}
