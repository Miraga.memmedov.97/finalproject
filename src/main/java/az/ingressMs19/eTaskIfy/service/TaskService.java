package az.ingressMs19.eTaskIfy.service;

import az.ingressMs19.eTaskIfy.dto.TaskDto;
import az.ingressMs19.eTaskIfy.dto.TaskRequest;
import az.ingressMs19.eTaskIfy.enums.TaskStatus;
import az.ingressMs19.eTaskIfy.exception.NotAllowedException;

import java.util.List;

public interface TaskService {
    void createTask(TaskRequest request);

    TaskDto getTaskById(Long taskId) throws NotAllowedException;

    List<TaskDto> getAllTasksByOrganizationId(Long organizationId);

    List<TaskDto> getAllTasksByUserId(Long userId);

    List<TaskDto> getAllTasksByStatus(TaskStatus status);

    void updateTask(Long taskId, TaskRequest request) throws NotAllowedException;

    void deleteTask(Long taskId) throws NotAllowedException;
}
