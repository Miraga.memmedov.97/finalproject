package az.ingressMs19.eTaskIfy.service;

import az.ingressMs19.eTaskIfy.dto.OrganizationDto;
import az.ingressMs19.eTaskIfy.dto.OrganizationRequest;

public interface OrganizationService {
    OrganizationDto getOrganizationById(Long id);

    void updateOrganization(Long id, OrganizationRequest request);
}
