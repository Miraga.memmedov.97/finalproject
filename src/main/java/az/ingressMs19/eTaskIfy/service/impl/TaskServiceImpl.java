package az.ingressMs19.eTaskIfy.service.impl;

import az.ingressMs19.eTaskIfy.dto.TaskDto;
import az.ingressMs19.eTaskIfy.dto.TaskRequest;
import az.ingressMs19.eTaskIfy.enums.TaskStatus;
import az.ingressMs19.eTaskIfy.exception.EntityNotFoundException;
import az.ingressMs19.eTaskIfy.exception.NotAllowedException;
import az.ingressMs19.eTaskIfy.model.Organization;
import az.ingressMs19.eTaskIfy.model.Task;
import az.ingressMs19.eTaskIfy.model.User;
import az.ingressMs19.eTaskIfy.repository.OrganizationRepository;
import az.ingressMs19.eTaskIfy.repository.TaskRepository;
import az.ingressMs19.eTaskIfy.repository.UserRepository;
import az.ingressMs19.eTaskIfy.service.TaskService;
import az.ingressMs19.eTaskIfy.service.auth.AuthService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class TaskServiceImpl implements TaskService {
    private final TaskRepository taskRepository;
    private final UserRepository userRepository;
    private final OrganizationRepository organizationRepository;
    private final AuthService authService;


    @Override
    public void createTask(TaskRequest request) {
        Organization organization = organizationRepository
                .findByUsersUserEmail(authService.getSignedInUser().getEmail())
                .orElseThrow(() -> new EntityNotFoundException("Organization not found"));


        Task task = Task.builder()
                .title(request.getTitle())
                .description(request.getDescription())
                .dateTime(request.getDeadline())
                .status(TaskStatus.PENDING)
                .organization(organization)
                .build();

        taskRepository.save(task);
        log.info("Task created successfully");

    }

    @Override
    public TaskDto getTaskById(Long taskId) throws NotAllowedException {
        return  mapTaskToDto(getByTaskId(taskId));
    }

    @Override
    public List<TaskDto> getAllTasksByOrganizationId(Long organizationId) {
        checkIfOrgMatches(organizationId);
        List<TaskDto> tasks = taskRepository.findAllByOrganizationId(organizationId)
                .stream()
                .map(this::mapTaskToDto)
                .collect(Collectors.toList());
        return tasks;
    }


    @Override
    public List<TaskDto> getAllTasksByUserId(Long assignedUsersId) {
        List<TaskDto> tasks = taskRepository.findAllByAssignedUsersId(assignedUsersId)
                .stream()
                .map(this::mapTaskToDto)
                .collect(Collectors.toList());
        return tasks;
    }

    @Override
    public List<TaskDto> getAllTasksByStatus(TaskStatus status) {
        List<TaskDto> tasks = taskRepository
                .findAllByOrganizationId(authService.getSignedInUser()
                        .getOrganization()
                        .getId())
                .stream()
                .filter(task -> task.getStatus().equals(status))
                .map(this::mapTaskToDto)
                .collect(Collectors.toList());
        return tasks;
    }

    @Override
    public void updateTask(Long taskId, TaskRequest request) throws NotAllowedException {
        Task task = getByTaskId(taskId);
        task = Task.builder()
                .id(taskId)
                .title(request.getTitle())
                .description(request.getDescription())
                .dateTime(request.getDeadline())
                .status(TaskStatus.valueOf(request.getStatus().toUpperCase()))
                .organization(task.getOrganization())
                .build();
        taskRepository.save(task);
    }

    @Override
    public void deleteTask(Long taskId) throws NotAllowedException {
        taskRepository.delete(getByTaskId(taskId));
    }

    private Task getByTaskId(Long taskId) throws NotAllowedException {
        Organization signedInUserOrganization = authService.getSignedInUser().getOrganization();
        Task task = taskRepository.findById(taskId)
                .orElseThrow(() -> new EntityNotFoundException("Task was not found"));

        if (!task.getOrganization().equals(signedInUserOrganization)) {
            throw new NotAllowedException("You are not allowed to access this task");
        }
        else
            return task;

    }

    private void checkIfOrgMatches(Long organizationId) {
        Long signedInUserOrgId = authService.getSignedInUser().getOrganization().getId();
        if (signedInUserOrgId != organizationId) {
            throw new NotAllowedException("You are not allowed to access this organization");
        }
    }

    private TaskDto mapTaskToDto(Task task) {
        return TaskDto.builder()
                .title(task.getTitle())
                .description(task.getDescription())
                .status(task.getStatus())
                .deadline(task.getDateTime())
                .build();
    }

}
