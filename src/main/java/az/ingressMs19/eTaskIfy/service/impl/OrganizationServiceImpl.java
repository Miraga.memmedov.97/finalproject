package az.ingressMs19.eTaskIfy.service.impl;

import az.ingressMs19.eTaskIfy.dto.OrganizationDto;
import az.ingressMs19.eTaskIfy.dto.OrganizationRequest;
import az.ingressMs19.eTaskIfy.exception.EntityNotFoundException;
import az.ingressMs19.eTaskIfy.exception.NotAllowedException;
import az.ingressMs19.eTaskIfy.model.Organization;
import az.ingressMs19.eTaskIfy.repository.OrganizationRepository;
import az.ingressMs19.eTaskIfy.service.OrganizationService;
import az.ingressMs19.eTaskIfy.service.auth.AuthService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor


public class OrganizationServiceImpl implements OrganizationService {

    private final OrganizationRepository organizationRepository;
    private final AuthService authService;
    private final ModelMapper modelMapper;

    @Override
    public OrganizationDto getOrganizationById(Long id) {
        return organizationRepository
                .findById(id)
                .map(org -> modelMapper.map(org, OrganizationDto.class))
                .orElseThrow(() -> new EntityNotFoundException("Organization not found"));
    }

    @Override
    public void updateOrganization(Long id, OrganizationRequest request) {
        Long loggedInAdmin = authService.getSignedInUser().getOrganization().getId();
        if (!loggedInAdmin.equals(id)) {
            throw new NotAllowedException("You are not allowed to update this organization");
        }
        Organization org = organizationRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Organization not found"));
        org = Organization.builder()
                .id(id)
                .name(request.getOrganizationName())
                .address(request.getAddress())
                .phoneNumber(request.getPhoneNumber())
                .users(org.getUsers())
                .tasks(org.getTasks())

                .build();
        organizationRepository.save(org);
    }

}
