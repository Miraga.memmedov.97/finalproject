package az.ingressMs19.eTaskIfy.service.impl;

import az.ingressMs19.eTaskIfy.dto.UserCreateResponse;
import az.ingressMs19.eTaskIfy.dto.UserDto;
import az.ingressMs19.eTaskIfy.dto.UserRequest;
import az.ingressMs19.eTaskIfy.enums.Role;
import az.ingressMs19.eTaskIfy.exception.AlreadyExistsException;
import az.ingressMs19.eTaskIfy.exception.EntityNotFoundException;
import az.ingressMs19.eTaskIfy.exception.NotAllowedException;
import az.ingressMs19.eTaskIfy.mapper.PasswordGenerator;
import az.ingressMs19.eTaskIfy.mapper.UserMapper;
import az.ingressMs19.eTaskIfy.model.User;
import az.ingressMs19.eTaskIfy.repository.UserRepository;
import az.ingressMs19.eTaskIfy.service.UserService;
import az.ingressMs19.eTaskIfy.service.auth.AuthService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor

public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final PasswordGenerator passwordGenerator;
    private final PasswordEncoder passwordEncoder;
    private final AuthService authService;
    private final UserMapper userMapper;


    @Override
    public UserCreateResponse addUser(UserRequest userRequest) {

        if (userRepository.existsByEmail(userRequest.getEmail())) {
            throw new AlreadyExistsException("User with this email already exists");
        }

        User currentAdmin = authService.getSignedInUser();

        User user = new User();
        user.setEmail(userRequest.getEmail());
        user.setName(userRequest.getName());
        user.setSurname(userRequest.getSurname());
        String password = passwordGenerator.generatePassword();
        user.setPassword(passwordEncoder.encode(password));
        user.setRole(Role.USER);
        user.setOrganization(currentAdmin.getOrganization());
        user.setUsername(userRequest.getUsername());
        userRepository.save(user);

        return UserCreateResponse.builder()
                .email(user.getEmail())
                .password(password)
                .build();
    }

    @Override
    public List<UserDto> getAllUsers() {
        User signedInUser = authService.getSignedInUser();
        List<UserDto> users = userRepository.findAllByOrganizationId(signedInUser
                        .getOrganization()
                        .getId())
                .stream()
                .map(userMapper::mapUserToUserDTO)
                .collect(Collectors.toList());
        return users;
    }

    @Override
    public UserDto getUserById(Long id) {
        User user = getUserOrThrow(userRepository.findById(id));
        return userMapper.mapUserToUserDTO(user);
    }

    @Override
    public UserDto getUserByEmail(String email) {
        User user = getUserOrThrow(userRepository.findByEmail(email));
        return userMapper.mapUserToUserDTO(user);
    }

    @Override
    public void deleteUserById(Long id) {
        getUserOrThrow(userRepository.findById(id));
        userRepository.deleteById(id);
    }

    @Override
    public void updateUserById(Long id, UserRequest userRequest) {
        User user = getUserOrThrow(userRepository.findById(id));
        user = User.builder()
                .id(user.getId())
                .email(userRequest.getEmail())
                .name(userRequest.getName())
                .surname(userRequest.getSurname())
                .password(user.getPassword())
                .role(user.getRole())
                .organization(user.getOrganization())
                .username(userRequest.getUsername())
                .tasks(user.getTasks())
                .build();
        userRepository.save(user);
    }

    private User getUserOrThrow(Optional<User> optionalUser) {
        Long SignedInUserOrgId = getCurrentOrganizationId();
        User user = optionalUser.orElseThrow(() -> new EntityNotFoundException("User not found"));
        if (!user.getOrganization().getId().equals(SignedInUserOrgId)) {
            throw new NotAllowedException("You are not allowed to access this user");
        }
        return user;
    }

    private Long getCurrentOrganizationId() {
        return authService.getSignedInUser()
                .getOrganization()
                .getId();
    }

}
