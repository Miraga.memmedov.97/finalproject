package az.ingressMs19.eTaskIfy.service.auth;

import az.ingressMs19.eTaskIfy.dto.authDto.AuthResponse;
import az.ingressMs19.eTaskIfy.dto.authDto.SignInRequest;
import az.ingressMs19.eTaskIfy.dto.authDto.SignUpRequest;
import az.ingressMs19.eTaskIfy.enums.Role;
import az.ingressMs19.eTaskIfy.exception.AlreadyExistsException;
import az.ingressMs19.eTaskIfy.exception.EntityNotFoundException;
import az.ingressMs19.eTaskIfy.exception.IncorrectCredentialsException;
import az.ingressMs19.eTaskIfy.model.Organization;
import az.ingressMs19.eTaskIfy.model.User;
import az.ingressMs19.eTaskIfy.repository.OrganizationRepository;
import az.ingressMs19.eTaskIfy.repository.UserRepository;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.List;

@Service
@RestControllerAdvice
@AllArgsConstructor
@Slf4j
public class AuthService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authManager;
    private final OrganizationRepository organizationRepository;
    private final UserDetailsServiceImpl userDetailsService;

    @Transactional
    public AuthResponse signUp(SignUpRequest signUpRequest) {

        List<String> errors = new ArrayList<>();

        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            errors.add("User with this username already exists");
        }
        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            errors.add("User with this email already exists");
        }
        if (organizationRepository.existsByName(signUpRequest.getOrganizationName())) {
            errors.add("Organization with this name already exists");
        }

        if (!errors.isEmpty()) {
            String errorMessage = String.join(", ", errors);
            throw new AlreadyExistsException(errorMessage);
        }

        Organization organization = Organization.builder()
                .name(signUpRequest.getOrganizationName())
                .address(signUpRequest.getOrganizationAddress())
                .phoneNumber(signUpRequest.getOrganizationPhoneNumber())
                .build();
        organizationRepository.save(organization);

        var user = User.builder()
                .username(signUpRequest.getUsername())
                .email(signUpRequest.getEmail())
                .password(passwordEncoder.encode(signUpRequest.getPassword()))
                .organization(organization)
                .role(Role.ADMIN)
                .build();
        userRepository.save(user);

        UserDetails userDetails = userDetailsService.loadUserByUsername(user.getEmail());
        String generatedToken = jwtService.generateToken(userDetails);
        log.info("User registered successfully with email: {}", user.getEmail());


        return AuthResponse.builder()
                .token(generatedToken)
                .build();
    }

    public AuthResponse signIn(SignInRequest request) {
        try {
            authManager.authenticate(
                    new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword())
            );

            UserDetails user = userDetailsService.loadUserByUsername(request.getEmail());
            String generatedToken = jwtService.generateToken(user);
            log.info("User signed in successfully with email: {}", user.getUsername());

            return AuthResponse.builder()
                    .token(generatedToken)
                    .message("Successfully signed in")
                    .build();

        } catch (AuthenticationException e) {
            throw new IncorrectCredentialsException("Incorrect username or password");
        }
    }

    public User getSignedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User signedInUser = userRepository
                .findByEmail(authentication.getName())
                .orElseThrow(() -> new EntityNotFoundException("User not found"));
        return signedInUser;
    }
}
