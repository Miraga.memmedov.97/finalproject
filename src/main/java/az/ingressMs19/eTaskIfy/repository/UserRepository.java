package az.ingressMs19.eTaskIfy.repository;

import az.ingressMs19.eTaskIfy.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Long> {
    Optional<User> findByEmail(String email);
    boolean existsByEmail(String email);
    boolean existsByUsername(String username);
    List<User> findAllByOrganizationId(Long id);
}
