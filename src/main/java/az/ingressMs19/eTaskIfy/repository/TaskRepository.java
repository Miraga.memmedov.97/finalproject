package az.ingressMs19.eTaskIfy.repository;

import az.ingressMs19.eTaskIfy.enums.TaskStatus;
import az.ingressMs19.eTaskIfy.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Long> {
    List<Task> findAllByAssignedUsersId(Long assignedUsersId);
    List<Task> findAllByStatus(TaskStatus status);
    List<Task> findAllByOrganizationId(Long organizationId);
}
