package az.ingressMs19.eTaskIfy.controller;

import az.ingressMs19.eTaskIfy.dto.UserCreateResponse;
import az.ingressMs19.eTaskIfy.dto.UserDto;
import az.ingressMs19.eTaskIfy.dto.UserRequest;
import az.ingressMs19.eTaskIfy.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class UserControllerTest {
    @Mock
    private UserService userService;

    @InjectMocks
    private UserController userController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void addUser_shouldReturnCreatedResponse() {
        // Given
        UserRequest userRequest = new UserRequest();
        UserCreateResponse expectedResponse = new UserCreateResponse();
        when(userService.addUser(userRequest)).thenReturn(expectedResponse);

        // When
        ResponseEntity<?> response = userController.addUser(userRequest);

        // Then
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(expectedResponse, response.getBody());
    }

    @Test
    void getAllUser_shouldReturnListOfUsers() {
        // Given
        List<UserDto> expectedUsers = Collections.singletonList(new UserDto());
        when(userService.getAllUsers()).thenReturn(expectedUsers);

        // When
        List<UserDto> actualUsers = userController.getAllUser();

        // Then
        assertEquals(expectedUsers, actualUsers);
    }

    @Test
    void getUserById_shouldReturnUser() {
        // Given
        long userId = 1L;
        UserDto expectedUser = new UserDto();
        when(userService.getUserById(userId)).thenReturn(expectedUser);

        // When
        UserDto actualUser = userController.getUserById(userId);

        // Then
        assertEquals(expectedUser, actualUser);
    }

    @Test
    void updateUser_shouldCallServiceMethod() {
        // Given
        long userId = 1L;
        UserRequest userRequest = new UserRequest();

        // When
        userController.updateUser(userId, userRequest);

        // Then
        verify(userService).updateUserById(userId, userRequest);
    }

    @Test
    void deleteUser_shouldCallServiceMethod() {
        // Given
        long userId = 1L;

        // When
        userController.deleteUser(userId);

        // Then
        verify(userService).deleteUserById(userId);
    }

}