package az.ingressMs19.eTaskIfy.controller;

import az.ingressMs19.eTaskIfy.dto.authDto.AuthResponse;
import az.ingressMs19.eTaskIfy.dto.authDto.SignInRequest;
import az.ingressMs19.eTaskIfy.dto.authDto.SignUpRequest;
import az.ingressMs19.eTaskIfy.exception.IncorrectCredentialsException;
import az.ingressMs19.eTaskIfy.service.auth.AuthService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class AuthorControllerTest {

    @Mock
    private AuthService authService;

    @InjectMocks
    private AuthorController authorController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSignUp() {
        SignUpRequest signUpRequest = new SignUpRequest(); // Create a SignUpRequest object
        AuthResponse expectedResponse = new AuthResponse(); // Create an expected response object
        when(authService.signUp(signUpRequest)).thenReturn(expectedResponse); // Mock the signUp method of AuthService

        ResponseEntity<AuthResponse> responseEntity = authorController.signUp(signUpRequest); // Call the signUp method of AuthorController

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode()); // Verify that status code is OK
        assertEquals(expectedResponse, responseEntity.getBody()); // Verify that response body matches expected response
        verify(authService, times(1)).signUp(signUpRequest); // Verify that signUp method of AuthService was called exactly once with the provided signUpRequest
    }

    @Test
    public void testSignIn() {
        SignInRequest signInRequest = new SignInRequest(); // Create a SignInRequest object
        AuthResponse expectedResponse = new AuthResponse(); // Create an expected response object
        when(authService.signIn(signInRequest)).thenReturn(expectedResponse); // Mock the signIn method of AuthService

        ResponseEntity<?> responseEntity = authorController.signIn(signInRequest); // Call the signIn method of AuthorController

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode()); // Verify that status code is OK
        assertEquals(expectedResponse, responseEntity.getBody()); // Verify that response body matches expected response
        verify(authService, times(1)).signIn(signInRequest); // Verify that signIn method of AuthService was called exactly once with the provided signInRequest
    }

    @Test
    public void testSignInIncorrectCredentialsException() {
        SignInRequest signInRequest = new SignInRequest(); // Create a SignInRequest object
        when(authService.signIn(signInRequest)).thenThrow(new IncorrectCredentialsException("Incorrect credentials")); // Mock the signIn method of AuthService to throw IncorrectCredentialsException

        ResponseEntity<?> responseEntity = authorController.signIn(signInRequest); // Call the signIn method of AuthorController

        assertEquals(HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode()); // Verify that status code is UNAUTHORIZED
        assertEquals("Incorrect credentials", responseEntity.getBody()); // Verify that response body contains the correct error message
        verify(authService, times(1)).signIn(signInRequest); // Verify that signIn method of AuthService was called exactly once with the provided signInRequest
    }
}
