package az.ingressMs19.eTaskIfy.controller;

import az.ingressMs19.eTaskIfy.dto.OrganizationDto;
import az.ingressMs19.eTaskIfy.dto.OrganizationRequest;
import az.ingressMs19.eTaskIfy.service.OrganizationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class OrganizationControllerTest {

    private MockMvc mockMvc;

    @Mock
    private OrganizationService organizationService;

    @InjectMocks
    private OrganizationController organizationController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(organizationController).build();
    }

    @Test
    public void testGetById() throws Exception {
        long id = 1L;
        OrganizationDto organizationDto = new OrganizationDto(); // create organizationDto with dummy data
        when(organizationService.getOrganizationById(id)).thenReturn(organizationDto);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/organizations/{id}", id))
                .andExpect(status().isOk())
                .andReturn();


    }

    @Test
    public void testUpdateOrganization() throws Exception {
        long id = 1L;
        OrganizationRequest organizationRequest = new OrganizationRequest(); // create organizationRequest with dummy data

        // Mocking behavior of service method
        // Assuming updateOrganization method returns void
        // If it returns something else, you need to mock accordingly
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/organizations/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"name\": \"New Name\" }") // Example JSON content for request body
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        // Assert other expectations if needed
    }
}
