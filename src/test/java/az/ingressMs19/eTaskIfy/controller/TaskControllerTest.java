package az.ingressMs19.eTaskIfy.controller;

import az.ingressMs19.eTaskIfy.dto.TaskDto;
import az.ingressMs19.eTaskIfy.dto.TaskRequest;
import az.ingressMs19.eTaskIfy.enums.TaskStatus;
import az.ingressMs19.eTaskIfy.service.TaskService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class TaskControllerTest {

    @Mock
    private TaskService taskService;

    @InjectMocks
    private TaskController taskController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void createTask() {
        TaskRequest taskRequest = new TaskRequest();
        doNothing().when(taskService).createTask(taskRequest);

        taskController.createTask(taskRequest);

        verify(taskService, times(1)).createTask(taskRequest);
    }

    @Test
    void updateTaskById() {
        long taskId = 1L;
        TaskRequest taskRequest = new TaskRequest();
        doNothing().when(taskService).updateTask(taskId, taskRequest);

        taskController.updateTaskById(taskId, taskRequest);

        verify(taskService, times(1)).updateTask(taskId, taskRequest);
    }

    @Test
    void deleteTaskById() {
        long taskId = 1L;
        doNothing().when(taskService).deleteTask(taskId);

        taskController.deleteTaskById(taskId);

        verify(taskService, times(1)).deleteTask(taskId);
    }

    @Test
    void getTaskById() {
        long taskId = 1L;
        TaskDto taskDto = new TaskDto();
        when(taskService.getTaskById(taskId)).thenReturn(taskDto);

        TaskDto result = taskController.getTaskById(taskId);

        assertEquals(taskDto, result);
        verify(taskService, times(1)).getTaskById(taskId);
    }

    @Test
    void getAllTasksByUserId() {
        long userId = 1L;
        List<TaskDto> taskList = Collections.singletonList(new TaskDto());
        when(taskService.getAllTasksByUserId(userId)).thenReturn(taskList);

        List<TaskDto> result = taskController.getAllTasksByUserId(userId);

        assertEquals(taskList, result);
        verify(taskService, times(1)).getAllTasksByUserId(userId);
    }

    @Test
    void getAllTaskByOrgId() {
        long orgId = 1L;
        List<TaskDto> taskList = Collections.singletonList(new TaskDto());
        when(taskService.getAllTasksByOrganizationId(orgId)).thenReturn(taskList);

        List<TaskDto> result = taskController.getAllTaskByOrgId(orgId);

        assertEquals(taskList, result);
        verify(taskService, times(1)).getAllTasksByOrganizationId(orgId);
    }

    @Test
    void getTasksByStatusId() {
        String status = "IN_PROGRESS";
        List<TaskDto> taskList = Collections.singletonList(new TaskDto());
        when(taskService.getAllTasksByStatus(TaskStatus.valueOf(status))).thenReturn(taskList);

        List<TaskDto> result = taskController.getTasksByStatusId(status);

        assertEquals(taskList, result);
        verify(taskService, times(1)).getAllTasksByStatus(TaskStatus.valueOf(status));
    }
}
