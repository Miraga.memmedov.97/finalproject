package az.ingressMs19.eTaskIfy.service.impl;

import az.ingressMs19.eTaskIfy.dto.UserCreateResponse;
import az.ingressMs19.eTaskIfy.dto.UserDto;
import az.ingressMs19.eTaskIfy.dto.UserRequest;
import az.ingressMs19.eTaskIfy.enums.Role;
import az.ingressMs19.eTaskIfy.exception.AlreadyExistsException;
import az.ingressMs19.eTaskIfy.exception.EntityNotFoundException;
import az.ingressMs19.eTaskIfy.exception.NotAllowedException;
import az.ingressMs19.eTaskIfy.mapper.UserMapper;
import az.ingressMs19.eTaskIfy.model.Organization;
import az.ingressMs19.eTaskIfy.model.User;
import az.ingressMs19.eTaskIfy.repository.UserRepository;
import az.ingressMs19.eTaskIfy.service.auth.AuthService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private AuthService authService;

    @Mock
    private UserMapper userMapper;

    @InjectMocks
    private UserServiceImpl userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void addUser_Success() {
        UserRequest userRequest = new UserRequest();
        userRequest.setEmail("test@example.com");
        userRequest.setName("Test");
        userRequest.setSurname("User");
        userRequest.setUsername("testuser");

        User currentAdmin = new User();
        currentAdmin.setId(1L);
        Organization organization = new Organization();
        organization.setId(1L);
        currentAdmin.setOrganization(organization);

        when(authService.getSignedInUser()).thenReturn(currentAdmin);
        when(userRepository.existsByEmail(userRequest.getEmail())).thenReturn(false);

        User savedUser = new User();
        savedUser.setId(2L);
        savedUser.setEmail(userRequest.getEmail());
        savedUser.setName(userRequest.getName());
        savedUser.setSurname(userRequest.getSurname());
        savedUser.setUsername(userRequest.getUsername());
        savedUser.setRole(Role.USER);
        savedUser.setOrganization(organization);
        when(userRepository.save(any(User.class))).thenReturn(savedUser);

        UserCreateResponse response = userService.addUser(userRequest);

        assertNotNull(response);
        assertEquals(userRequest.getEmail(), response.getEmail());
        assertNotNull(response.getPassword());
    }

    // Write other test methods for other functionalities like getAllUsers, getUserById, etc.
}
