package az.ingressMs19.eTaskIfy.service.impl;

import az.ingressMs19.eTaskIfy.dto.TaskDto;
import az.ingressMs19.eTaskIfy.dto.TaskRequest;
import az.ingressMs19.eTaskIfy.enums.TaskStatus;
import az.ingressMs19.eTaskIfy.exception.EntityNotFoundException;
import az.ingressMs19.eTaskIfy.exception.NotAllowedException;
import az.ingressMs19.eTaskIfy.model.Organization;
import az.ingressMs19.eTaskIfy.model.Task;
import az.ingressMs19.eTaskIfy.repository.OrganizationRepository;
import az.ingressMs19.eTaskIfy.repository.TaskRepository;
import az.ingressMs19.eTaskIfy.repository.UserRepository;
import az.ingressMs19.eTaskIfy.service.auth.AuthService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class TaskServiceImplTest {

    @Mock
    private TaskRepository taskRepository;
    @Mock
    private UserRepository userRepository;
    @Mock
    private OrganizationRepository organizationRepository;
    @Mock
    private AuthService authService;

    @InjectMocks
    private TaskServiceImpl taskService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testCreateTask() {
        TaskRequest request = new TaskRequest();
        request.setTitle("Test Task");
        request.setDescription("This is a test task");
        request.setDeadline(LocalDateTime.now());

        Organization organization = new Organization();
        organization.setId(1L);

        when(organizationRepository.findByUsersUserEmail(anyString())).thenReturn(Optional.of(organization));

        taskService.createTask(request);

        verify(taskRepository, times(1)).save(any(Task.class));
    }

    @Test
    void testGetTaskById() throws NotAllowedException {
        Task task = new Task();
        task.setId(1L);
        task.setTitle("Test Task");
        task.setDescription("This is a test task");
        LocalDateTime now = LocalDateTime.now();
        task.setDateTime(now);
        task.setStatus(TaskStatus.PENDING);

        when(taskRepository.findById(1L)).thenReturn(Optional.of(task));

        TaskDto taskDto = taskService.getTaskById(1L);

        assertEquals(task.getTitle(), taskDto.getTitle());
        assertEquals(task.getDescription(), taskDto.getDescription());
        assertEquals(task.getStatus(), taskDto.getStatus());
        assertEquals(task.getDateTime(), taskDto.getDeadline());
    }

    // Add more test cases for other methods...

}
