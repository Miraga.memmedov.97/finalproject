package az.ingressMs19.eTaskIfy.service.impl;

import az.ingressMs19.eTaskIfy.dto.OrganizationDto;
import az.ingressMs19.eTaskIfy.dto.OrganizationRequest;
import az.ingressMs19.eTaskIfy.exception.EntityNotFoundException;
import az.ingressMs19.eTaskIfy.exception.NotAllowedException;
import az.ingressMs19.eTaskIfy.model.Organization;
import az.ingressMs19.eTaskIfy.repository.OrganizationRepository;
import az.ingressMs19.eTaskIfy.service.auth.AuthService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class OrganizationServiceImplTest {

    @Mock
    private OrganizationRepository organizationRepository;

    @Mock
    private AuthService authService;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private OrganizationServiceImpl organizationService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getOrganizationById_OrganizationExists_OrganizationDtoReturned() {

        Long id = 1L;
        Organization organization = new Organization();
        organization.setId(id);
        OrganizationDto organizationDto = new OrganizationDto();
        organizationDto.setId(id);

        when(organizationRepository.findById(id)).thenReturn(Optional.of(organization));
        when(modelMapper.map(organization, OrganizationDto.class)).thenReturn(organizationDto);


        OrganizationDto result = organizationService.getOrganizationById(id);


        assertNotNull(result);
        assertEquals(id, result.getId());
    }

    @Test
    void getOrganizationById_OrganizationNotExists_EntityNotFoundExceptionThrown() {

        Long id = 1L;

        when(organizationRepository.findById(id)).thenReturn(Optional.empty());


        assertThrows(EntityNotFoundException.class, () -> organizationService.getOrganizationById(id));
    }

    @Test
    void updateOrganization_ValidIdAndRequest_OrganizationUpdated() {

        Long id = 1L;
        OrganizationRequest request = new OrganizationRequest();
        request.setOrganizationName("New Organization Name");
        request.setAddress("New Address");
        request.setPhoneNumber("New Phone Number");

        Organization organization = new Organization();
        organization.setId(id);

        //when(authService.getSignedInUser()).thenReturn(organization);
        when(organizationRepository.findById(id)).thenReturn(Optional.of(organization));


        organizationService.updateOrganization(id, request);


        verify(organizationRepository, times(1)).save(organization);
        assertEquals(request.getOrganizationName(), organization.getName());
        assertEquals(request.getAddress(), organization.getAddress());
        assertEquals(request.getPhoneNumber(), organization.getPhoneNumber());
    }

    @Test
    void updateOrganization_InvalidId_NotAllowedExceptionThrown() {

        Long id = 1L;
        Long loggedInAdminId = 2L;
        OrganizationRequest request = new OrganizationRequest();

        Organization org = new Organization();
        org.setId(loggedInAdminId);

        //when(authService.getSignedInUser()).thenReturn(org);


        assertThrows(NotAllowedException.class, () -> organizationService.updateOrganization(id, request));
        verify(organizationRepository, never()).findById(anyLong());
        verify(organizationRepository, never()).save(any(Organization.class));
    }
}
